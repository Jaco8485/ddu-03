﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreesETrigger : MonoBehaviour
{
    public Canvas pressE;
    public Canvas samtale;

    public GameObject UI;

    UIController UICon;

    private void Start()
    {
        UICon = UI.GetComponent<UIController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        pressE.gameObject.SetActive(true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            samtale.gameObject.SetActive(true);
            UICon.InventoryTjek();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        pressE.gameObject.SetActive(false);
    }


}
