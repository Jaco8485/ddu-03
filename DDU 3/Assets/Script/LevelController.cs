﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public void GoToGame(string sceneName)
    {
        Application.LoadLevel(sceneName);
    }

    public void ExitMenu()
    {
        Application.Quit();
    }
}
