﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 2;

    public int morale = 0;

    public GameObject player;
    public GameObject playerMesh;

    public Transform samtalCam;
    public Transform camPos;

    public bool[] inventory = new bool[4];

    public Animator anim;

    public PlayerManger pM;

    //bool canMove = true;
    bool canMove = false;

    Rigidbody rb;

    Vector3 pos;

    Quaternion playerRotasion;

    Camera camera1;

    // Start is called before the first frame update
    void Start()
    {
        camera1 = GetComponentInChildren<Camera>();

        rb = GetComponent<Rigidbody>();

        playerRotasion = playerMesh.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Blend",morale);

        if (canMove)
        {
            Move();
            camera1.transform.position = camPos.position;
        }
        else
        {
            camera1.transform.position = samtalCam.position;
            anim.SetBool("IsWalking", false);
        }
    }


    private void Move()
    {
        pos = player.transform.position;

        float hor = Input.GetAxis("Horizontal");

        pos = pos + new Vector3(hor * speed, 0, 0);

        player.transform.position = pos;

        if (hor < 0)
        {
            Quaternion newRotasion = playerRotasion;

            newRotasion.y = -playerRotasion.y;

            playerMesh.transform.rotation = newRotasion;

            anim.SetBool("IsWalking",true);
        }
        else if (hor == 0)
        {
            anim.SetBool("IsWalking", false);
        }
        else
        {
            playerMesh.transform.rotation = playerRotasion;

            anim.SetBool("IsWalking", true);
        }
    }

    public void CanMove(bool b)
    {
        canMove = b;
    }

    public void Inventory(int i)
    {
        inventory[i-1] = true;
    }



}
