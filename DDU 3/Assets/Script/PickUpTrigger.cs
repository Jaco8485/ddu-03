﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTrigger : MonoBehaviour
{
    int count = 0;

    public int pickUpNumber = 1;

    public Canvas pressE;

    public GameObject pickUp;

    private void OnTriggerEnter(Collider other)
    {
        if (count <= 1)
        {
            count++;
        }

        if (count == 2)
        {
            pressE.gameObject.SetActive(true);
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            pressE.gameObject.SetActive(false);
            other.gameObject.GetComponent<PlayerController>().Inventory(pickUpNumber);

            Destroy(pickUp);
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        pressE.gameObject.SetActive(false);
    }
}
