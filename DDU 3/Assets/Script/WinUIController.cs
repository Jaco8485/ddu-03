﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinUIController : MonoBehaviour
{
    public GameObject gameManger;

    public int samtaleNumber;

    PlayerManger getPlayer;
    PlayerController playerController;

    GameObject player;

    private int morale;

    public GameObject trigger;    

    public Canvas good;
    public Canvas neutral;
    public Canvas bad;
    public Canvas all;
    public Canvas scene;

    public Text sceneText1;
    public Text sceneText2;
    public Text upperText;
    public Text lowerText;

    private void Start()
    {
        getPlayer = gameManger.GetComponent<PlayerManger>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
        
    }

    public void EnterEndScreen()
    {
        morale = playerController.morale;
        scene.gameObject.SetActive(true);
        if (morale < 0)
        {
            sceneText1.text = "You got to the hospital and as you get to your mothers room, you see alot of doctors.";
            sceneText2.text = "A doctor walks towards you and says'I'm sorry, but your too late.'";
        }
        else if (morale > 0)
        {
            sceneText1.text = "You got to the hospital in time and your mother smiled towards you as you enters the room.";
            sceneText2.text = "'You seem happy, I guess you have done something nice today', she said as she closed her eyes and slowly fell asleep.";
        }
        else
        {
            sceneText1.text = "You got to the hospital in time and your mother smiled towards you as you enters the room.";
            sceneText2.text = "Your mother doesn't even seem to notice you.";
        }
    }

    public void CheckMoraleStatus()
    {
        scene.gameObject.SetActive(false);
        if (morale < 0)
        {
            upperText.text = "The Bad Ending";
            lowerText.text = "You didn't get to say goodbye to your mother.";
            upperText.color = Color.red;
            lowerText.color = Color.red;
            bad.gameObject.SetActive(true);
            all.gameObject.SetActive(true);
        }
        else if (morale > 0)
        {
            upperText.text = "The Good Ending";
            lowerText.text = "You got to say goodbye to your mother.";
            upperText.color = Color.yellow;
            lowerText.color = Color.yellow;
            lowerText.fontSize = 80;
            good.gameObject.SetActive(true);
            all.gameObject.SetActive(true);
        }
        else 
        {
            upperText.text = "The Neutral Ending";
            lowerText.text = "Your mother doesn't even seem to notice you.";
            upperText.color = Color.white;
            lowerText.color = Color.white;
            neutral.gameObject.SetActive(true);
            all.gameObject.SetActive(true);
        }
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitLevel()
    {
        SceneManager.LoadScene("StartScene");
    }
}
