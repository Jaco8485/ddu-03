﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public Canvas canvas;
    public GameObject UI;

    UIController UICon;

    private void Start()
    {
        UICon = UI.GetComponent<UIController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<PlayerController>().CanMove(false);
        UICon.InventoryTjek();

        canvas.gameObject.SetActive(true);
    }
}
