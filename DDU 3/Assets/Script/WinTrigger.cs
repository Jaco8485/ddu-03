﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTrigger : MonoBehaviour
{
    public GameObject UI;

    WinUIController UICon;

    private void Start()
    {
        UICon = UI.GetComponent<WinUIController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<PlayerController>().CanMove(false);
        UICon.EnterEndScreen();
    }
}
