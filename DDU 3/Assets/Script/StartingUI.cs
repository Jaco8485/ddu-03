﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingUI : MonoBehaviour
{
    public GameObject gameManger;

    PlayerManger getPlayer;
    PlayerController playerController;
    GameObject player;

    public Canvas startC;

    private void Start()
    {
        getPlayer = gameManger.GetComponent<PlayerManger>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
    }

    public void BeginningGame()
    {
        playerController.CanMove(true);
        startC.gameObject.SetActive(false);
    }
}
