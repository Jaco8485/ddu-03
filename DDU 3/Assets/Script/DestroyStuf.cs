﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyStuf : MonoBehaviour
{
    public List<GameObject> objectsToDestroy;

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i <= objectsToDestroy.Count-1; i++)
        {
            objectsToDestroy[i].SetActive(false);
        }
    }


}
