﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public int maxX = -2;
    public int minX = 10;

    public float speed = 3;

    public GameObject npcMesh;

    Animator anim;

    Vector3 pos;

    Quaternion npcRotasion;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        npcRotasion = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        if (transform.position.x > maxX)
        {
            speed = -speed;
        }

        if (transform.position.x < minX)
        {
            speed = -speed;
        }
    }

    private void Move()
    {
        pos = transform.position;

        pos = pos + new Vector3(speed, 0, 0);

        transform.position = pos;

        if (speed < 0)
        {
            Quaternion newRotasion = npcRotasion;

            newRotasion.y = 180;

            transform.rotation = newRotasion;
        }
        else
        {
            transform.rotation = npcRotasion;
        }
    }
}
