﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public GameObject gameManger;

    PlayerManger getPlayer;
    PlayerController playerController;

    GameObject player;

    public int samtaleNumber;

    public GameObject butten;

    public Canvas canvas;
    public Canvas pressE;

    public GameObject trigger;
    public GameObject newTrigger;

    private void Start()
    {
        getPlayer = gameManger.GetComponent<PlayerManger>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
    }

    public void InventoryTjek()
    {
        if (playerController.inventory[samtaleNumber - 1] == true)
        {
            butten.SetActive(true);
        }
        else
        {
            butten.SetActive(false);
        }
    }


    public void CanPlayerMove()
    {
        trigger.SetActive(false);
        newTrigger.SetActive(true);

        canvas.gameObject.SetActive(false);

        playerController.CanMove(true);
    }

    public void UsePickUp()
    {
        playerController.inventory[samtaleNumber-1] = false;
        pressE.gameObject.SetActive(false);
        Destroy(newTrigger);
    }

    public void EndSamtale()
    {
        pressE.gameObject.SetActive(false);
        Destroy(newTrigger);
    }

    public void BadChoice()
    {
        int morale = playerController.morale - 1;
        playerController.morale = morale;
        Debug.Log("morale: " + morale);
    }

    public void GoodChoice()
    {
        int morale = playerController.morale + 1;
        playerController.morale = morale;
        Debug.Log("morale: " + morale);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
