﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManger : MonoBehaviour
{
    public GameObject player;
    public GameObject oldman;
    public GameObject dogo;
    public GameObject kvide;
    public GameObject overfalder;

    public GameObject GivePlayer()
    {
        return player;
    }

    public GameObject GiveOldman()
    {
        return oldman;
    }

    public GameObject GiveDogo()
    {
        return dogo;
    }


    public GameObject GiveKvide()
    {
        return kvide;
    }

    public GameObject GiveOverfalder()
    {
        return overfalder;
    }


    public void OldMan()
    {
        Animator oldmanAnim = oldman.GetComponent<Animator>();

        oldmanAnim.SetBool("Bool", true);
    }

    public void Dogo()
    {
        Animator dogoAnim = dogo.GetComponent<Animator>();

        dogoAnim.SetBool("IsHappy", true);
    }

    public void Overfald()
    {
        Animator kvideAnim = kvide.GetComponent<Animator>();
        Animator overfalderAnim = overfalder.GetComponent<Animator>();

        Quaternion rot = kvide.transform.rotation;
        rot.y = -150;
        kvide.transform.rotation = rot;

        overfalderAnim.SetBool("Fald", true);
        kvideAnim.SetBool("Help", true);
    }
}
